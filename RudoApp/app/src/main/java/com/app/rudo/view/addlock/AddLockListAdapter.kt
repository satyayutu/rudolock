package com.app.rudo.view.addlock

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.app.rudo.R
import com.app.rudo.databinding.LockAddListItemBinding
import com.ttlock.bl.sdk.api.ExtendedBluetoothDevice
import java.util.*

/*
// Created by Satyabrata Bhuyan on 14-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

class AddLockListAdapter(private val mContext: Context) :
    RecyclerView.Adapter<AddLockListAdapter.DeviceViewHolder>() {

    private val mDataList =
        LinkedList<ExtendedBluetoothDevice>()
    private var mListener: onLockItemClick? = null

    //  private var mContext: Activity? = null
    private val TIMEOUT = 5000
    private val mAddStatusList =
        LinkedList<ExtendedBluetoothDevice>()
    private val mNormalStatusList =
        LinkedList<ExtendedBluetoothDevice>()
    private var lastSyncTimeStamp: Long = 0
    // private var mListener: onLockItemClick? = null

    interface onLockItemClick {
        fun onClick(device: ExtendedBluetoothDevice?)
    }

    fun setOnLockItemClick(click: onLockItemClick?) {
        mListener = click
    }


    @Synchronized
    fun updateData(device: ExtendedBluetoothDevice?) {
        if (device != null) {
            if (device.isSettingMode) {
                addOrSortLock(device, mAddStatusList)
                removeOtherStatusLock(device, mNormalStatusList)
            } else {
                addOrSortLock(device, mNormalStatusList)
                removeOtherStatusLock(device, mAddStatusList)
            }
            val currentTime = System.currentTimeMillis()
            if (currentTime - lastSyncTimeStamp >= 800) {
                if (!mDataList.isEmpty()) {
                    mDataList.clear()
                }
                mDataList.addAll(0, mAddStatusList)
                mDataList.addAll(mNormalStatusList)
                notifyDataSetChanged()
                lastSyncTimeStamp = currentTime
            }
        }
    }


    /**
     * you can sort the lock that be discovered by signal value.
     */
    private fun addOrSortLock(
        scanDevice: ExtendedBluetoothDevice,
        lockList: LinkedList<ExtendedBluetoothDevice>
    ) {
        var isContained = false
        var length = lockList?.size
        val mTopOneDevice: ExtendedBluetoothDevice
        scanDevice?.date = System.currentTimeMillis()
        if (length > 0) {
            mTopOneDevice = lockList[0]
            for (i in 0 until length) {
                val currentDevice = lockList[i]
                if (scanDevice.address == currentDevice.address) {
                    isContained = true
                    if (i != 0 && scanDevice.rssi > mTopOneDevice.rssi) {
                        lockList.removeAt(i)
                        lockList.add(0, scanDevice)
                    } else {
                        currentDevice.date = System.currentTimeMillis()
                        lockList[i] = currentDevice
                    }
                } else {
                    if (System.currentTimeMillis() - currentDevice.date >= TIMEOUT) {
                        lockList.removeAt(i)
                        length = lockList.size
                    }
                }
            }
            if (!isContained) {
                if (scanDevice.rssi > mTopOneDevice.rssi) {
                    lockList.add(0, scanDevice)
                } else {
                    lockList.add(scanDevice)
                }
            }
        } else {
            lockList.add(scanDevice)
        }
    }

    /**
     * the lock mode will be changed,so should update the list when lock mode changed.
     * @param scanDevice the lock that be discovered.
     */
    private fun removeOtherStatusLock(
        scanDevice: ExtendedBluetoothDevice,
        lockList: LinkedList<ExtendedBluetoothDevice>
    ) {
        if (!lockList.isEmpty()) {
            var length = lockList.size
            for (i in 0 until length) {
                val device = lockList[i]
                if (device.address == scanDevice.address) {
                    lockList.removeAt(i)
                    length--
                } else {
                    if (System.currentTimeMillis() - device.date >= TIMEOUT) {
                        lockList.removeAt(i)
                        length--
                    }
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DeviceViewHolder {
        val mView: View =
            LayoutInflater.from(mContext).inflate(R.layout.lock_add_list_item, parent, false)
        return DeviceViewHolder(mView, mListener!!)
    }

    override fun onBindViewHolder(_holder: DeviceViewHolder, position: Int) {
        val item = mDataList[position]
        _holder.bindItem(item)
    }

    override fun getItemCount(): Int {
        return mDataList.size
    }

    class DeviceViewHolder(itemView: View?, private val mListener: onLockItemClick) :
        RecyclerView.ViewHolder(itemView!!) {
        private var itemBinding: LockAddListItemBinding? = DataBindingUtil.bind(itemView!!)

        fun bindItem(item: ExtendedBluetoothDevice) {
            itemBinding?.tvLockName?.text = item.name
            itemBinding?.ivSettingMode?.visibility =
                if (item.isSettingMode) View.VISIBLE else View.GONE

            itemView?.rootView?.setOnClickListener {
                if (item.isSettingMode) {
                    mListener.onClick(item)
                }
            }
        }
    }
}
