package com.app.rudo.view.customersupport

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.app.rudo.repository.HomeRepository
import com.app.rudo.utils.AppPrefrences

/*
// Created by Satyabrata Bhuyan on 06-08-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

class SupportViewModelFactory (
    private val repository: HomeRepository,
    private val pref: AppPrefrences
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return SupportViewModel(repository,pref) as T
    }
}