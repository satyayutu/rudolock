package com.app.rudo.network

import com.app.rudo.utils.ApiException
import com.app.rudo.utils.NoInternetException
import kotlinx.coroutines.Deferred
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Response
import timber.log.Timber

abstract class ApiRequest {

    suspend fun <T : Any> apiRequest(call: suspend () -> Deferred<Response<T>>): T {
        try {
            val response = call.invoke().await()
            if (response.isSuccessful) {
                return response.body()!!
            } else {
                val error = response.errorBody().toString()
                val message = StringBuilder()
                error?.let {
                    try {
                        message.append(JSONObject(it).getString("error"))
                    } catch (e: JSONException) {
                    }
                    message.append("\n")
                }
                message.append("Error Code: ${response.code()}")
                throw ApiException(message.toString())
            }
        } catch (network: NoInternetException) {
            return throw network
        } catch (e: Exception) {
            Timber.e("API error message", e.message)
            return throw ApiException("API error")
        }
    }
}