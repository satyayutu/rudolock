package com.app.rudo.view.gateway

import android.Manifest
import android.annotation.TargetApi
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.rudo.R
import com.app.rudo.databinding.FragmentAddGatewayBinding
import com.ttlock.bl.sdk.api.ExtendedBluetoothDevice
import com.ttlock.bl.sdk.api.TTLockClient
import com.ttlock.bl.sdk.gateway.api.GatewayClient
import com.ttlock.bl.sdk.gateway.callback.ScanGatewayCallback
import kotlinx.android.synthetic.main.fragment_add_lock.*
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

/*
// Created by Satyabrata Bhuyan on 23-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

class AddGatewayFragment: Fragment(), KodeinAware {
    override val kodein: Kodein by kodein()
    private val REQUEST_PERMISSION_REQ_CODE = 11
    private val factory: GatewayViewmodelFactory by instance<GatewayViewmodelFactory>()
    private var mAdapter: GatewayListAdapter? = null
    private var viewModel: GatewayViewmodel? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding: FragmentAddGatewayBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_add_gateway, container, false)
        viewModel = ViewModelProvider(this, factory).get(GatewayViewmodel::class.java)
        binding.gateway = viewModel

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rv_lock_list.also {
            it.layoutManager = LinearLayoutManager(requireContext())
            it.setHasFixedSize(true)
            mAdapter = GatewayListAdapter(requireContext())
           // mAdapter?.setOnLockItemClick(this)
            it.adapter = mAdapter

        }
        GatewayClient.getDefault().prepareBTService(requireContext())
        initListener()
    }


    private fun initListener() {
        val isBtEnable =
            GatewayClient.getDefault().isBLEEnabled(requireContext())
        if (isBtEnable) {
            startScan()
        } else {
            GatewayClient.getDefault().requestBleEnable(requireActivity())
        }
    }

    /**
     * before call startScanGateway,the location permission should be granted.
     */
    @TargetApi(Build.VERSION_CODES.M)
    private fun startScan() {
        if (ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(
                arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION), REQUEST_PERMISSION_REQ_CODE)
            return
        }
        getScanGatewayCallback()
    }

    /**
     * start scan BT lock
     */
    private fun getScanGatewayCallback() {
        GatewayClient.getDefault().startScanGateway(object : ScanGatewayCallback {
            override fun onScanGatewaySuccess(device: ExtendedBluetoothDevice) {
//                LogUtil.d("device:" + device);
                 mAdapter?.updateData(device)
            }

            override fun onScanFailed(errorCode: Int) {}
        })
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults.size == 0) {
            return
        }
        when (requestCode) {
            REQUEST_PERMISSION_REQ_CODE -> {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getScanGatewayCallback()
                } else {
                    if (permissions[0] == Manifest.permission.ACCESS_COARSE_LOCATION) {
                    }
                }
            }
            else -> {
            }
        }
    }

     override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == GatewayClient.REQUEST_ENABLE_BT && requestCode == Activity.RESULT_OK) {
            startScan()
        }
    }
    override fun onDestroyView() {
        super.onDestroyView()
        /**
         * BT service should be released before Activity finished.
         */
        TTLockClient.getDefault().stopBTService()
    }


}