package com.app.rudo.view.lockdetails.passcode

/*
// Created by Satyabrata Bhuyan on 08-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

interface GeneratePasscodeImpl {
    fun onStarted()
    fun onSuccess(message: String)
    fun onFailure(message: String)
}