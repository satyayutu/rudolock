package com.app.rudo.view.lockdetails

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.findNavController
import com.app.rudo.BuildConfig
import com.app.rudo.R
import com.app.rudo.model.PassCodeListModel
import com.app.rudo.model.ekeys.EAccessKey
import com.app.rudo.model.locklist.Lock
import com.app.rudo.repository.LockDetailRepository
import com.app.rudo.utils.ApiException
import com.app.rudo.utils.NoInternetException
import kotlinx.coroutines.launch
import java.net.URLDecoder

class LockDetailsViewModel(
    private val repository: LockDetailRepository
) : ViewModel() {
    private var lockDeatilsImpl: LockDeatilsImpl? = null
    private var selectedLockId: Int? = null
    var passCodeListModel:PassCodeListModel? = null
    var eAccessKey:EAccessKey? = null
   // var keyData:KeyData? = null
   var lock: Lock? = null
    fun registerListner(lockDeatilsImpl: LockDeatilsImpl) {
        this.lockDeatilsImpl = lockDeatilsImpl
    }

    fun setSelectedLockId(lockId: Int) {
        selectedLockId = lockId
    }

    fun onClickGiveAccess(view: View) {
        val args = Bundle()
        args.putSerializable("lockData",lock!!)
        view.findNavController()
            .navigate(R.id.action_lockDetailsFragment_to_giveAccessFragment, args)
    }

    fun onClickGeneratePassCode(view: View) {
        //val args = bundleOf("lock" to selectedLockId)
        val args = Bundle()
        args.putSerializable("lockData",lock!!)
        view.findNavController().navigate(R.id.action_lockDetailsFragment_to_passcodeFragment, args)
    }

    fun onClickAccessHistory(view: View) {
        val args = bundleOf("lock" to selectedLockId)
        view.findNavController().navigate(R.id.action_lockDetailsFragment_to_accessHistory, args)
    }

    fun onClickPasscodeHistory(view: View) {
        val args = bundleOf("lock" to selectedLockId)
        view.findNavController().navigate(R.id.action_lockDetailsFragment_to_passcodeHistory, args)
    }

    fun onClickIcCard(view: View) {
        //val args = bundleOf("lock" to selectedLockId)
        val args = Bundle()
        args.putSerializable("lockData",lock!!)
        args.putString("type" , "IcCard")
        view.findNavController().navigate(R.id.action_lockDetailsFragment_to_iccardFragment, args)

    }

    fun onClickFingerPrint(view: View) {
        val args = Bundle()
        args.putSerializable("lockData",lock!!)
        args.putString("type" , "Fingerprint")
        view.findNavController().navigate(R.id.action_lockDetailsFragment_to_iccardFragment, args)
    }

    fun onClickRecords(view: View) {
        val args = bundleOf("lock" to selectedLockId)
        view.findNavController().navigate(R.id.action_lockDetailsFragment_to_records, args)
    }

    fun onClickSettings(view: View) {

        //val args = bundleOf("lock" to selectedLockId)
        val args = Bundle()
        args.putSerializable("lockData", lock)
        view.findNavController().navigate(R.id.action_lockDetailsFragment_to_settings, args)
    }

    fun onClickLock(view: View) {
       // lockDeatilsImpl?.onClickLockImage()
    }


    fun getLockList(lockId: Int) {
        if (BuildConfig.IS_OPENTT_URL_CALL) {
            keyListFromOpenTT(lockId)
        } else {
            keyListFromYutu(lockId)
        }
    }
    private fun keyListFromYutu(lockId: Int) {
        lockDeatilsImpl?.onStarted()
        viewModelScope.launch {
            try {
                val result = repository.getKeyList(lockId)
                result.let {
                   // lock = it
                    lockDeatilsImpl?.onSuccesLockDetails(it)
                }
            } catch (e: ApiException) {
                lockDeatilsImpl?.onFailure(e.message!!)
            } catch (e: NoInternetException) {
                lockDeatilsImpl?.onFailure(e.message!!)
            }
        }
    }

    private fun keyListFromOpenTT(lockId: Int) {
        viewModelScope.launch {
            try {
                val result = repository.getKeyListFromOpenTTApi(lockId)
                result.let {
                   // keyData = it
                    lockDeatilsImpl?.onSuccesLockDetails(it)
                }
            } catch (e: ApiException) {
                lockDeatilsImpl?.onFailure(e.message!!)
            } catch (e: NoInternetException) {
                lockDeatilsImpl?.onFailure(e.message!!)
            }
        }
    }

    fun getLockKey(lockId: Int) {
        lockDeatilsImpl?.onStarted()
        if (BuildConfig.IS_OPENTT_URL_CALL) {
            getLockKeyFromOpenTTApi(lockId)
        } else {
            getLockKeyFromYutu(lockId)
        }
    }

    private fun getLockKeyFromYutu(lockId: Int) {
        viewModelScope.launch {
            try {
                val result = repository.getLockKey(lockId)
                result.let {
                    if (!it.list.isNullOrEmpty()) {
                        it.list.filter { it.username.isNotEmpty() }.forEach {
                            it.username = it.username.split("_")[1]
                        }
                        lockDeatilsImpl?.onSuccessLockKey(it.list)
                    }else{
                        it.errmsg.let {
                            lockDeatilsImpl?.onFailure(it!!)
                        }
                    }
                }

            } catch (e: ApiException) {
                lockDeatilsImpl?.onFailure(e.message!!)
            } catch (e: NoInternetException) {
                lockDeatilsImpl?.onFailure(e.message!!)
            }
        }
    }

    private fun getLockKeyFromOpenTTApi(lockId: Int) {
        viewModelScope.launch {
            try {
                val result = repository.getLockKeyOpenTTApi(lockId)
                result.let {
                    if (it.list != null && it.list.size >0) {
                        it.list.filter { it.username.isNotEmpty() }.forEach {
                            if (it.username.contains("_")) {
                                it.username = it.username.split("_")[1]
                            }
                        }
                        lockDeatilsImpl?.onSuccessLockKey(it.list)
                    }else{
                        it.errmsg.let {
                            lockDeatilsImpl?.onFailure(it!!)
                        }
                    }
                }

            } catch (e: ApiException) {
                lockDeatilsImpl?.onFailure(e.message!!)
            } catch (e: NoInternetException) {
                lockDeatilsImpl?.onFailure(e.message!!)
            }
        }
    }
    fun getPassCodes(lockId: Int) {
        lockDeatilsImpl?.onStarted()
        if (BuildConfig.IS_OPENTT_URL_CALL) {
            getPassCodesFromOpenTTApi(lockId)
        } else {
            getPassCodesFromYutu(lockId)
        }
    }

    private fun getPassCodesFromYutu(lockId: Int) {
        viewModelScope.launch {
            try {
                val result = repository.getPassCodes(lockId)
                result.let {
                    lockDeatilsImpl?.onSuccessPassCodes(it.list!!)
                }
            } catch (e: ApiException) {
                lockDeatilsImpl?.onFailure(e.message!!)
            } catch (e: NoInternetException) {
                lockDeatilsImpl?.onFailure(e.message!!)
            }
        }
    }

    private fun getPassCodesFromOpenTTApi(lockId: Int) {
        viewModelScope.launch {
            try {
                val result = repository.getPassCodesOpenTTApi(lockId)
                result.let {
                    if(!it.list.isNullOrEmpty()) {
                        lockDeatilsImpl?.onSuccessPassCodes(it.list!!)
                    }else{
                        it.errmsg.let {
                            lockDeatilsImpl?.onFailure(it!!)
                        }
                    }
                }

            } catch (e: ApiException) {
                lockDeatilsImpl?.onFailure(e.message!!)
            } catch (e: NoInternetException) {
                lockDeatilsImpl?.onFailure(e.message!!)
            }
        }
    }
    fun uploadRecords(logs:String){
       // lockDeatilsImpl?.onStarted()
        val decoded: String = URLDecoder.decode(logs, "UTF-8")

        if (BuildConfig.IS_OPENTT_URL_CALL) {
            uploadRecodesOpenTTApi(decoded)
        } else {
            uploadRecodes(decoded)
        }
    }
    private fun uploadRecodes(logs: String) {
        viewModelScope.launch {
            try {
                val result = repository.uploadRecodes(selectedLockId!!,logs)
                result.let {
                    if(it.errmsg.isNullOrEmpty()){
                        lockDeatilsImpl?.onSuccess()
                    }else{
                        lockDeatilsImpl?.onFailure(it.errmsg)
                    }

                }

            } catch (e: ApiException) {
                lockDeatilsImpl?.onFailure(e.message!!)
            } catch (e: NoInternetException) {
                lockDeatilsImpl?.onFailure(e.message!!)
            }
        }
    }
    private fun uploadRecodesOpenTTApi(logs: String) {
        viewModelScope.launch {
            try {
                val result = repository.uploadRecodesOpenTTApi(selectedLockId!!,logs)
                result.let {
                    lockDeatilsImpl?.onSuccess()
                }

            } catch (e: ApiException) {
                lockDeatilsImpl?.onFailure(e.message!!)
            } catch (e: NoInternetException) {
                lockDeatilsImpl?.onFailure(e.message!!)
            }
        }
    }

    fun getUnlockRecords(){
        lockDeatilsImpl?.onStarted()
        if (BuildConfig.IS_OPENTT_URL_CALL) {
            getUnlockRecodesOpenTTApi()
        } else {
            getUnlockRecodes()
        }
    }
    private fun getUnlockRecodesOpenTTApi() {
        viewModelScope.launch {
            try {
                val result = repository.getUnloackRecodsOpenTTApi(selectedLockId!!)
                result.let {
                    lockDeatilsImpl?.onSuccessUnlockRecords(it.list)
                }

            } catch (e: ApiException) {
                lockDeatilsImpl?.onFailure(e.message!!)
            } catch (e: NoInternetException) {
                lockDeatilsImpl?.onFailure(e.message!!)
            }
        }
    }
    private fun getUnlockRecodes() {
        viewModelScope.launch {
            try {
                val result = repository.getUnloackRecods(selectedLockId!!)
                result.let {
                    lockDeatilsImpl?.onSuccessUnlockRecords(it.list)
                }

            } catch (e: ApiException) {
                lockDeatilsImpl?.onFailure(e.message!!)
            } catch (e: NoInternetException) {
                lockDeatilsImpl?.onFailure(e.message!!)
            }
        }
    }

    fun deleteeAccessKey(eAccessKey: EAccessKey){
        if(BuildConfig.IS_OPENTT_URL_CALL){
            deleteeAccessKeyOpenTT(eAccessKey)
        }else{
            deleteeAccessKeyYutu(eAccessKey)
        }
    }
    private fun deleteeAccessKeyOpenTT(eAccessKey: EAccessKey) {
        viewModelScope.launch {
            try {
                val result = repository.deleteeAccessKeyOpneTT(eAccessKey.keyId)
                result.let {
                  getLockKey(eAccessKey.lockId)
                }

            } catch (e: ApiException) {
                lockDeatilsImpl?.onFailure(e.message!!)
            } catch (e: NoInternetException) {
                lockDeatilsImpl?.onFailure(e.message!!)
            }
        }
    }
    private fun deleteeAccessKeyYutu(eAccessKey: EAccessKey) {
        viewModelScope.launch {
            try {
                val result = repository.deleteeAccessKey(eAccessKey.keyId)
                result.let {
                    getLockKey(eAccessKey.lockId)
                }

            } catch (e: ApiException) {
                lockDeatilsImpl?.onFailure(e.message!!)
            } catch (e: NoInternetException) {
                lockDeatilsImpl?.onFailure(e.message!!)
            }
        }
    }

    fun deletePasscode(passcodeData: PassCodeListModel){
        if(BuildConfig.IS_OPENTT_URL_CALL){
            deletePasscodeOpenTT(passcodeData)
        }else{
            deletePasscodeYutu(passcodeData)
        }
    }
    private fun deletePasscodeOpenTT(passcodeData: PassCodeListModel) {
        viewModelScope.launch {
            try {
                val result = repository.deletePasscodeOpneTT(passcodeData.lockId!!,passcodeData.keyboardPwdId!!)
                result.let {
                    getPassCodes(passcodeData.lockId)
                }

            } catch (e: ApiException) {
                lockDeatilsImpl?.onFailure(e.message!!)
            } catch (e: NoInternetException) {
                lockDeatilsImpl?.onFailure(e.message!!)
            }
        }
    }
    private fun deletePasscodeYutu(passcodeData: PassCodeListModel) {
        viewModelScope.launch {
            try {
                val result = repository.deletePasscode(passcodeData.lockId!!,passcodeData.keyboardPwdId!!)
                result.let {
                    getPassCodes(passcodeData.lockId)
                }

            } catch (e: ApiException) {
                lockDeatilsImpl?.onFailure(e.message!!)
            } catch (e: NoInternetException) {
                lockDeatilsImpl?.onFailure(e.message!!)
            }
        }
    }
    fun deletePasscodeFromDetails(view: View){
        deletePasscode(passCodeListModel!!)
    }
    fun deleteEAccessFromDetails(view: View){
        deleteeAccessKey(eAccessKey!!)
    }

}