package com.app.rudo.view.lockdetails.access

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.app.rudo.R
import com.app.rudo.model.locklist.Lock
import kotlinx.android.synthetic.main.fragment_give_access.*
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein

class GiveAccessFragment : Fragment(),KodeinAware {

    override val kodein: Kodein by kodein()
   // private val factory: GiveAccessViewModelFactory by instance<GiveAccessViewModelFactory>()
    private var lock:Lock? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_give_access, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
       // val viewModel = ViewModelProvider(this,factory).get(GiveAccessViewModel::class.java)

        arguments?.let {
            lock =(Lock::class.java).cast( arguments?.getSerializable("lockData"))
        }
        setPageAdpter()
    }

    private fun setPageAdpter(){
        val adpter = GiveAccessPageAdapter(childFragmentManager)
        adpter.addFragment(GivePermanetFragment(lock?.lockId!!),"Permanent")
        adpter.addFragment(GiveOneTimeFragment(lock?.lockId!!),"One Time")
        adpter.addFragment(GiveTimedFragment(lock?.lockId!!),"Timed")
        viewpagerBase.adapter = adpter
    }

}