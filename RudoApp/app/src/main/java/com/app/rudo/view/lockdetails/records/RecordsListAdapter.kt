package com.app.rudo.view.lockdetails.records

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.app.rudo.R
import com.app.rudo.databinding.UnlockListItemBinding
import com.app.rudo.model.recodes.RecodeDetailsModel

/*
// Created by Satyabrata Bhuyan on 10-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

class RecordsListAdapter(
    val listData : List<RecodeDetailsModel>
): RecyclerView.Adapter<RecordsListAdapter.PassCodeHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)=PassCodeHolder(
        DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.unlock_list_item,
            parent,
            false
        )
    )

    override fun getItemCount() = listData.size

    override fun onBindViewHolder(holder: PassCodeHolder, position: Int) {
        holder.itemUnlockBinding.unlockrecords = listData[position]
    }

    inner class PassCodeHolder(
        val itemUnlockBinding: UnlockListItemBinding
    ): RecyclerView.ViewHolder(itemUnlockBinding.root)

}