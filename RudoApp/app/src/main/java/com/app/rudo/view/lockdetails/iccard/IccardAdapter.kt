package com.app.rudo.view.lockdetails.iccard

import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.app.rudo.R
import com.app.rudo.databinding.IccardItemBinding
import com.app.rudo.model.iccard.IccardData

/*
// Created by Satyabrata Bhuyan on 11-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

class IccardAdapter(
    val listData : MutableList<IccardData>,
    val listner : ItemClickListner
): RecyclerView.Adapter<IccardAdapter.IccardHolder>(){
    private var itemsPendingRemoval: MutableList<IccardData>? = mutableListOf()
    private var PENDING_REMOVAL_TIMEOUT: Long = 3000
    var handler: Handler? = Handler()
    var pendingRunnables: HashMap<IccardData, Runnable>? = HashMap()
    init {
        itemsPendingRemoval = mutableListOf<IccardData>()
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)=IccardHolder(
        DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.iccard_item,
            parent,
            false
        )
    )

    override fun getItemCount() = listData.size

    override fun onBindViewHolder(holder: IccardHolder, position: Int) {
        holder.itemIccard.iccardvm = listData[position]

        if (itemsPendingRemoval!!.contains(listData[position])) {
            //show swipe layout
            holder.itemIccard.swipeLayout.visibility = View.VISIBLE
            holder.itemIccard.itemRoot.visibility = View.GONE

            holder.itemIccard.txtUndo.setOnClickListener({ view ->
                undoOpt(listData[position])
            })
            holder.itemIccard.txtDelete.setOnClickListener({ view ->
                listner?.deleteOnClick(listData[position])
               // remove(position)
            })

        } else {
            //show regular layout
            holder.itemIccard.swipeLayout.visibility = View.GONE
            holder.itemIccard.itemRoot.visibility = View.VISIBLE

           /* itemView.txt.text = model.name
            itemView.sub_txt.text = model.version
            val id = context.resources.getIdentifier(model.name.toLowerCase(), "drawable", context.packageName)
            itemView.img.setBackgroundResource(id)*/
        }

    }
    fun undoOpt(model: IccardData) {
        val pendingRemovalRunnable: Runnable? = pendingRunnables?.get(model)
        pendingRunnables?.remove(model)
        if (pendingRemovalRunnable != null)
            handler?.removeCallbacks(pendingRemovalRunnable)
        itemsPendingRemoval?.remove(model)
        // this will rebind the row in "normal" state
        notifyItemChanged(listData.indexOf(model))
    }
    inner class IccardHolder(
        val itemIccard: IccardItemBinding
    ): RecyclerView.ViewHolder(itemIccard.root)
    fun pendingRemoval(position: Int) {

        val data = listData.get(position)
        if (!itemsPendingRemoval!!.contains(data)) {
            itemsPendingRemoval?.add(data)
            // this will redraw row in "undo" state
            notifyItemChanged(position)
            // let's create, store and post a runnable to remove the data
           /* val pendingRemovalRunnable = Runnable {
                remove(listData.indexOf(data))
            }

            handler?.postDelayed(pendingRemovalRunnable, PENDING_REMOVAL_TIMEOUT)
            // pendingRunnables!![data] = pendingRemovalRunnable
            pendingRunnables?.put(data, pendingRemovalRunnable)*/
        }
    }

    fun remove(position: Int) {
        val data = listData.get(position)
        if (itemsPendingRemoval!!.contains(data)) {
            itemsPendingRemoval?.remove(data)
        }
        if (listData.contains(data)) {
            //dataList.remove(position)
            listData.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    fun isPendingRemoval(position: Int): Boolean {
        val data = listData.get(position)
        return itemsPendingRemoval!!.contains(data)
    }

}