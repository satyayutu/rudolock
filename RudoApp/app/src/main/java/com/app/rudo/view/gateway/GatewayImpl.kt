package com.app.rudo.view.gateway

import com.app.rudo.model.gateway.AccList

/*
// Created by Satyabrata Bhuyan on 23-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

interface GatewayImpl {
    fun onStarted()
    fun onFailure(message:String)
    fun OnGatewayAccItem(list : List<AccList>)
}