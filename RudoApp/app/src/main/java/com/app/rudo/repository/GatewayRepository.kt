package com.app.rudo.repository

import com.app.rudo.BuildConfig
import com.app.rudo.model.gateway.GatewayAccList
import com.app.rudo.network.ApiRequest
import com.app.rudo.network.api.RudoApi
import com.app.rudo.utils.AppPrefrences

/*
// Created by Satyabrata Bhuyan on 23-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

class GatewayRepository(
    private val rudoApi: RudoApi,
    private val appPrefrences: AppPrefrences
) : ApiRequest() {


    suspend fun getGatewayAccList(): GatewayAccList {
        return apiRequest { rudoApi.getGatewayAccountList(
            BuildConfig.CLIENT_ID,
            appPrefrences.getAccessToken(),
            1,
            20,
            System.currentTimeMillis(),
            BuildConfig.REDIRECT_OPENTT_URI+"/v3/gateway/list"
        )!! }
    }
    suspend fun getGatewayAccListOpenTT(): GatewayAccList {
        return apiRequest { rudoApi.getGatewayAccountListOpenTT(
            BuildConfig.CLIENT_ID,
            appPrefrences.getAccessToken(),
            1,
            20,
            System.currentTimeMillis()
        )!! }
    }
}