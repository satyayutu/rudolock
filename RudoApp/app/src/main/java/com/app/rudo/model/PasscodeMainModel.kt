package com.app.rudo.model

/*
// Created by Satyabrata Bhuyan on 19-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

data class PasscodeMainModel(
    val list: List<PassCodeListModel>?,
    val pageNo: Int?,
    val pageSize: Int?,
    val pages: Int?,
    val total: Int?,
    val errcode :Int?,
    val errmsg :String?,
    val description:String?
)
