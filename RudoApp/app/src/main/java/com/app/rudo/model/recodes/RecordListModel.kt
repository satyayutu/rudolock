package com.app.rudo.model.recodes

/*
// Created by Satyabrata Bhuyan on 17-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

data class RecordListModel(
    val list:List<RecodeDetailsModel>,
    val pageNo:	Int?,
    val pageSize:Int?,
    val pages:	Int?,
    val total:	Int?
)

