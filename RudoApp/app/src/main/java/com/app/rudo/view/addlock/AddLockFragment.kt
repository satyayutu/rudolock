package com.app.rudo.view.addlock

import android.Manifest
import android.annotation.TargetApi
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.rudo.R
import com.app.rudo.databinding.FragmentAddLockBinding
import com.app.rudo.utils.hide
import com.app.rudo.utils.show
import com.ttlock.bl.sdk.api.ExtendedBluetoothDevice
import com.ttlock.bl.sdk.api.TTLockClient
import com.ttlock.bl.sdk.callback.InitLockCallback
import com.ttlock.bl.sdk.callback.ScanLockCallback
import com.ttlock.bl.sdk.callback.SetNBServerCallback
import com.ttlock.bl.sdk.constant.Feature
import com.ttlock.bl.sdk.entity.LockError
import com.ttlock.bl.sdk.util.SpecialValueUtil
import kotlinx.android.synthetic.main.fragment_add_lock.*
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance
import timber.log.Timber

class AddLockFragment : Fragment(), KodeinAware, AddLockListAdapter.onLockItemClick {
    override val kodein: Kodein by kodein()
    private val REQUEST_PERMISSION_REQ_CODE = 11
    private val factory: AddLockViewmodelFactory by instance<AddLockViewmodelFactory>()
    private var mAdapter: AddLockListAdapter? = null
    private var viewModel: AddLockViewModel? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding: FragmentAddLockBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_add_lock, container, false)
        viewModel = ViewModelProvider(this, factory).get(AddLockViewModel::class.java)
        binding.addlock = viewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rv_lock_list.also {
            it.layoutManager = LinearLayoutManager(requireContext())
            it.setHasFixedSize(true)
            mAdapter = AddLockListAdapter(requireContext())
            mAdapter?.setOnLockItemClick(this)
            it.adapter = mAdapter

        }

        initListener()
    }


    /**
     * prepareBTService should be called first,or all TTLock SDK function will not be run correctly
     */
    private fun initBtService() {
        TTLockClient.getDefault().prepareBTService(requireContext())


    }


    private fun initListener() {
        initBtService()
        val isBtEnable = TTLockClient.getDefault().isBLEEnabled(requireContext())
        if (!isBtEnable) {
            TTLockClient.getDefault().requestBleEnable(requireActivity())
        }
        startScan()
        //getScanLockCallback()


    }

    private fun getScanLockCallback() {
        TTLockClient.getDefault().startScanLock(object : ScanLockCallback {
            override fun onScanLockSuccess(device: ExtendedBluetoothDevice) {
                // System.out.println("list available: "+device.getName());
                /* if (mListApapter != null) {
                     mListApapter.updateData(device)
                     // onClick(device);
                 }*/
                Timber.i("Scan result %s", device)
                mAdapter?.updateData(device)
                // onClick(device)
            }

            override fun onFail(error: LockError) {}
        })
    }

    /**
     * before call startScanLock,the location permission should be granted.
     */
    @TargetApi(Build.VERSION_CODES.M)
    private fun startScan() {
        if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_COARSE_LOCATION)
            != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(
                arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION), REQUEST_PERMISSION_REQ_CODE
            )
            return
        }
        if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION)
            != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), REQUEST_PERMISSION_REQ_CODE
            )
            return
        }

        getScanLockCallback()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults.size == 0) {
            return
        }
        when (requestCode) {
            REQUEST_PERMISSION_REQ_CODE -> {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getScanLockCallback()
                } else {
                    if (permissions[0] == Manifest.permission.ACCESS_COARSE_LOCATION) {
                    }
                }
            }
            else -> {
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        /**
         * BT service should be released before Activity finished.
         */
        TTLockClient.getDefault().stopBTService()
    }

    override fun onClick(device: ExtendedBluetoothDevice?) {
        /**
         * lockData: the server api lockData param need
         * isNBLock: is a NB-IoT lock.
         */
        progress_bar.show()
        TTLockClient.getDefault().initLock(device, object : InitLockCallback {
            override fun onInitLockSuccess(lockData: String, specialValue: Int) {
                //this must be done after lock is initialized,call server api to post to your server
                if (SpecialValueUtil.isSupportFeature(specialValue, Feature.NB_LOCK)) {
                    setNBServerForNBLock(lockData, device!!.address)
                } else {
                    // setAlias4LockAlert()
                    // upload2Server(lockData)
                    progress_bar.hide()
                    val args = bundleOf("lockData" to lockData)
                    view?.findNavController()
                        ?.navigate(R.id.action_addlocklist_to_setDeviceName, args)
                }
            }

            override fun onFail(error: LockError) {
                // makeErrorToast(error)
            }
        })
    }

    /**
     * if a NB-IoT lock you'd better do set NB-IoT server before upload lockData to server to active NB-IoT lock service.
     * And no matter callback is success or fail,upload lockData to server.
     * @param lockData
     * @param lockMac
     */
    private fun setNBServerForNBLock(
        lockData: String,
        lockMac: String
    ) {
        //NB server port
        val mNBServerPort: Short = 8011
        val mNBServerAddress = "192.127.123.11"
        TTLockClient.getDefault().setNBServerInfo(
            mNBServerPort,
            mNBServerAddress,
            lockData,
            lockMac,
            object : SetNBServerCallback {
                override fun onSetNBServerSuccess(battery: Int) {
                    //view?.rootView?.snackbar("--set NB server success--")
                    // setAlias4LockAlert()
                    // upload2Server(lockData)
                    val args = bundleOf("lockData" to lockData)
                    view?.findNavController()
                        ?.navigate(R.id.action_addlocklist_to_setDeviceName, args)
                }

                override fun onFail(error: LockError) {
                    // makeErrorToast(error)
                    //no matter callback is success or fail,upload lockData to server.
                    //setAlias4LockAlert()
                    // upload2Server(lockData)
                    val args = bundleOf("lockData" to lockData)
                    view?.findNavController()
                        ?.navigate(R.id.action_addlocklist_to_setDeviceName, args)
                }
            })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        /**
         * BT service should be released before Activity finished.
         */
        TTLockClient.getDefault().stopBTService()
    }
}