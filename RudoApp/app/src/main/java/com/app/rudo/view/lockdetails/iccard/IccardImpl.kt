package com.app.rudo.view.lockdetails.iccard

import com.app.rudo.model.fingerprint.FingerprintData
import com.app.rudo.model.iccard.IccardData

/*
// Created by Satyabrata Bhuyan on 11-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

interface IccardImpl {

    fun hideProgress()
    fun onStated()
    fun onSuccess(list:List<IccardData>)
    fun onFailure(message:String)
    fun onClickOkButton()
    fun addCardSuccess(lis:Int, type:String)
    fun fingerPrintList(list:List<FingerprintData>)
    fun onClickDelete(type: String)
}