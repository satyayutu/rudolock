package com.app.rudo.view.lockdetails.access

import android.view.View
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.app.rudo.BuildConfig
import com.app.rudo.contrants.Constants
import com.app.rudo.repository.LockDetailRepository
import com.app.rudo.utils.ApiException
import com.app.rudo.utils.NoInternetException
import kotlinx.coroutines.launch

class GiveAccessViewModel(
    private val repository: LockDetailRepository
) : ViewModel() {

    var lockId: Int? = null
    var recipient: String? = null
    var name: String? = null
    var startTime: Long = 0L
    var endTime: Long = 0L
    var remoteAllow: Int = 2
    private var listner: GiveAccessImpl? = null

    fun registerListner(giveAccessImpl: GiveAccessImpl) {
        listner = giveAccessImpl
    }

    fun onClickSwitAllow(view: View) {
        remoteAllow = if (view.isSelected) {
            1
        } else {
            2
        }
    }

    fun onClickSwitAuthorized(view: View) {

    }

    fun onClickSendButton(view: View) {
        listner?.onStarted()
        if (recipient.isNullOrEmpty()) {
            listner?.onFailure("Recipient can not be empty")
            return
        }
        /*if (name.isNullOrEmpty()) {
            listner?.onFailure("Name is empty")
             return
        }*/
        if (BuildConfig.IS_OPENTT_URL_CALL) {
            sendAccessKeyToOpenTTApi(lockId!!)
        } else {
            sendAccessKeyToYutu(lockId!!)
        }
    }

    private fun sendAccessKeyToOpenTTApi(lockId: Int) {
        viewModelScope.launch {
            try {
                val result = repository.sendAccessKeyToOpenTTApi(
                    lockId,
                    Constants.PREFIX_USERNAME+recipient!!,
                    name!!,
                    startTime,
                    endTime,
                    remoteAllow
                )
                result.let {
                    /*if (it.errcode != null && it.errcode == 0){
                        listner?.onSuccess()
                    }else {
                        listner?.onFailure(it.errmsg!!)
                    }*/
                    result.keyId?.let {
                        authorizeEAccessOpenTT(lockId,result.keyId!!)
                    }

                }
            } catch (e: ApiException) {
                listner?.onFailure(e.message!!)
            } catch (e: NoInternetException) {
                listner?.onFailure(e.message!!)
            }
        }
    }

    private fun sendAccessKeyToYutu(lockId: Int) {
        viewModelScope.launch {
            try {
                val result = repository.sendAccessKey(
                    lockId,
                    Constants.PREFIX_USERNAME+recipient!!,
                    name!!,
                    startTime,
                    endTime,
                    remoteAllow!!
                )
                result?.let {
                    if (it.errcode != null && it.errcode == 0){
                        listner?.onSuccess()
                    }else {
                        listner?.onFailure(it.errmsg!!)
                    }
                }

            } catch (e: ApiException) {
                listner?.onFailure(e.message!!)
            } catch (e: NoInternetException) {
                listner?.onFailure(e.message!!)
            }
        }
    }

    private fun authorizeEAccessOpenTT(lockId: Int,keyId: Int){
        viewModelScope.launch {
            try {
                val result = repository.authorizeEAccessOpenTT(
                    lockId,
                    keyId
                )
                result?.let {
                    if (it.errcode != null && it.errcode == 0){
                        listner?.onSuccess()
                    }else {
                        listner?.onFailure(it.errmsg!!)
                    }
                }

            } catch (e: ApiException) {
                listner?.onFailure(e.message!!)
            } catch (e: NoInternetException) {
                listner?.onFailure(e.message!!)
            }
        }
    }
}