package com.app.rudo.model.gateway

data class GatewayAccList(
    val list: List<AccList>
)