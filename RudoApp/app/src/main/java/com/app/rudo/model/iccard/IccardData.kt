package com.app.rudo.model.iccard

/*
// Created by Satyabrata Bhuyan on 11-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

data class IccardData(
    var cardId: Int?,
    var lockId: Int?,
    var cardNumber: String?,
    var cardName: String?,
    var startDate: Long?,
    var endDate: Long?,
    var createDate: Long?
)

