package com.app.rudo.model.ekeys

import java.io.Serializable

data class EAccessKey(
    val date: Long,
    val endDate: Long,
    val keyId: Int,
    val keyName: String,
    val keyRight: Int,
    val keyStatus: String,
    val lockId: Int,
    val openid: Int,
    val remarks: String,
    var senderUsername: String,
    val startDate: Long,
    var username: String,
    var type:String?
):Serializable