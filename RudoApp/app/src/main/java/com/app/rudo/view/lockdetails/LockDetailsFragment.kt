package com.app.rudo.view.lockdetails

import android.Manifest
import android.app.Dialog
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.app.rudo.R
import com.app.rudo.contrants.Constants
import com.app.rudo.databinding.FragmentLockDetailsBindingImpl
import com.app.rudo.model.PassCodeListModel
import com.app.rudo.model.eaccess.EAccessKeyList
import com.app.rudo.model.ekeys.EAccessKey
import com.app.rudo.model.lockdetails.KeyData
import com.app.rudo.model.locklist.Lock
import com.app.rudo.model.recodes.RecodeDetailsModel
import com.app.rudo.utils.*
import com.bumptech.glide.Glide
import com.ttlock.bl.sdk.api.TTLockClient
import com.ttlock.bl.sdk.callback.ControlLockCallback
import com.ttlock.bl.sdk.callback.GetLockTimeCallback
import com.ttlock.bl.sdk.callback.GetOperationLogCallback
import com.ttlock.bl.sdk.callback.SetLockTimeCallback
import com.ttlock.bl.sdk.constant.ControlAction
import com.ttlock.bl.sdk.constant.Feature
import com.ttlock.bl.sdk.constant.LogType
import com.ttlock.bl.sdk.entity.LockError
import com.ttlock.bl.sdk.util.SpecialValueUtil
import kotlinx.android.synthetic.main.fragment_lock_details.*
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance
import timber.log.Timber
import java.net.URLDecoder

class LockDetailsFragment : Fragment(), LockDeatilsImpl, DialogUitls.OnClickDialogItemImpl,
    KodeinAware {
    private val REQUEST_PERMISSION_REQ_CODE = 11
    override val kodein: Kodein by kodein()
    private val factory: LockDetailsViewmodelFactory by instance<LockDetailsViewmodelFactory>()
    private var viewModel: LockDetailsViewModel? = null
    private var keyData: KeyData? = null
    private var dialog: AlertDialog? = null
    var lock:Lock? = null
    var eAccessKey:EAccessKeyList? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentLockDetailsBindingImpl =
            DataBindingUtil.inflate(inflater, R.layout.fragment_lock_details, container, false)
        viewModel = ViewModelProvider(this, factory).get(LockDetailsViewModel::class.java)
        binding.viewmodel = viewModel
        viewModel?.registerListner(this)
        ensureBluetoothIsEnabled()
        if(viewModel?.lock != null){
            lock = viewModel?.lock
        }else{
            arguments?.let {
                eAccessKey =(EAccessKeyList::class.java).cast(arguments?.getSerializable("lockData"))
                lock = Lock(0,
                    eAccessKey?.electricQuantity,
                    eAccessKey?.keyboardPwdVersion,
                    eAccessKey?.lockAlias,
                    eAccessKey?.lockId,
                    eAccessKey?.noKeyPwd,
                    eAccessKey?.lockMac,
                    eAccessKey?.lockName,
                    eAccessKey?.specialValue,
                    eAccessKey?.lockData)
                lock?.let {
                    binding.lock = lock
                    viewModel?.lock = lock
                    viewModel?.setSelectedLockId(lock?.lockId!!)
                    // viewModel?.getLockList(lock?.lockId!!)
                }
            }
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        startScan()
        featureEnabled()
        onClickImagaLock()

    }

    override fun onStarted() {
        progress_bar?.show()
    }

    override fun onSuccess() {
        progress_bar?.hide()
    }

    override fun onFailure(message: String) {
        view?.rootView?.snackbar(message)

    }

    override fun onClickYes(dialog: Dialog) {
        dialog.cancel()
    }

    override fun onClickNo(dialog: Dialog) {
        dialog.cancel()
    }

    override fun onSuccesLockDetails(lockDetail: KeyData) {
       /* keyData = lockDetail
        progress_bar?.hide()
        keyData?.lockAlias?.let {
            val decoded: String = URLDecoder.decode(it, "UTF-8")
            deviceName.text = decoded
            featureEnabled()
        }
*/
    }

    private fun featureEnabled() {
        lock.let {
            val decoded: String = URLDecoder.decode(lock?.lockAlias, "UTF-8")
            deviceName.text = decoded
            val fp = SpecialValueUtil.isSupportFeature(
                lock?.specialValue!!,
                Feature.FINGER_PRINT.toInt()
            )
            val ic = SpecialValueUtil.isSupportFeature(lock?.specialValue!!, Feature.IC.toInt())

            if (fp) {
                fingerPrint.visibility = View.VISIBLE
            }
            if (ic) {
                iccardLinerlayout.visibility = View.VISIBLE
            }
        }
    }

/*    override fun onClickLockImage() {

    }*/

    private fun onClickImagaLock(){
        lockUnlock.setOnClickListener {
            doLock()
        }
        lockUnlock.setOnLongClickListener {
            doUnLock()
            return@setOnLongClickListener true
        }
    }
    override fun onSuccessLockKey(lockKeyList: List<EAccessKey>) {

    }

    override fun onSuccessPassCodes(lockKeyList: List<PassCodeListModel>) {

    }

    override fun onSuccessUnlockRecords(list: List<RecodeDetailsModel>) {

    }


    private fun doUnLock() {
        Glide.with(requireContext())
        .load(R.drawable.animat_lock)
        .into(lockUnlock)
        ensureBluetoothIsEnabled()
        TTLockClient.getDefault().controlLock(
            ControlAction.UNLOCK,
            lock?.lockData,
            lock?.lockMac,
            object : ControlLockCallback {
                override fun onControlLockSuccess(
                    lockAction: Int,
                    battery: Int,
                    uniqueId: Int
                ) {
                    Glide.with(requireContext())
                        .load(R.drawable.ic_lock_blue)
                        .into(lockUnlock)
                    dialog = showAlertDialog {
                        txtMessage.visibility = View.GONE
                        eText.text = "Unlock"
                        btnClickListener {
                            getRecords4mLock()
                            dialog?.cancel()
                        }
                    }
                    dialog?.show()

                }

                override fun onFail(error: LockError) {
                    Glide.with(requireContext())
                        .load(R.drawable.ic_lock_blue)
                        .into(lockUnlock)

                    view?.rootView?.snackbar("Unlock Failed")
                }
            })
    }

    private fun doLock() {
        TTLockClient.getDefault().controlLock(
            ControlAction.LOCK,
            lock?.lockData,
            lock?.lockMac,
            object : ControlLockCallback {
                override fun onControlLockSuccess(
                    lockAction: Int,
                    battery: Int,
                    uniqueId: Int
                ) {
                    view?.rootView?.snackbar("lock is locked!")
                }

                override fun onFail(error: LockError) {
                    //Toast.makeText(getActivity(),"lock lock fail!--" + error.getDescription(),Toast.LENGTH_LONG).show();
                    Timber.i(
                        "Lock list",
                        "lock is not locked!" + error.description
                    )
                }
            })

    }

    private fun ensureBluetoothIsEnabled() {
        if (!TTLockClient.getDefault().isBLEEnabled(context)) {
            TTLockClient.getDefault().requestBleEnable(activity)
        }
       // setLockTime()

    }
    private fun getLockTime(){
        TTLockClient.getDefault().getLockTime(
            lock?.lockData,
            lock?.lockMac,
            object : GetLockTimeCallback{
                override fun onFail(p0: LockError?) {

                }

                override fun onGetLockTimeSuccess(p0: Long) {
                    Constants.LOCKTIME = p0
                }

            }
        )
    }
    private fun setLockTime(){
        TTLockClient.getDefault().setLockTime(
            System.currentTimeMillis(),
            lock?.lockData,
            lock?.lockMac,
            object : SetLockTimeCallback {
                override fun onSetTimeSuccess() {
                    //makeToast("lock time is corrected")
                    getLockTime()
                }

                override fun onFail(error: LockError?) {
                   // makeErrorToast(error)
                }
            })
    }
    private fun getRecords4mLock() {
        lock?.let {
            TTLockClient.getDefault().getOperationLog(
                LogType.NEW,
                lock?.lockData,
                lock?.lockMac,
                object : GetOperationLogCallback {
                    @RequiresApi(Build.VERSION_CODES.O)
                    override fun onGetLogSuccess(log: String) {
                        //  val encodedString: String = Base64.getEncoder().encodeToString(log.toByteArray())
                        Timber.i("Json Data %s",log)
                        viewModel?.uploadRecords(log)
                    }

                    override fun onFail(error: LockError) {
                        view?.rootView?.snackbar(error.errorMsg)
                    }
                })
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        /**
         * BT service should be released before Activity finished.
         */
        TTLockClient.getDefault().stopBTService()
    }
    private fun startScan() {
        if (ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(
                arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION), REQUEST_PERMISSION_REQ_CODE
            )
        }
        if (ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), REQUEST_PERMISSION_REQ_CODE
            )

        }
    }

}

