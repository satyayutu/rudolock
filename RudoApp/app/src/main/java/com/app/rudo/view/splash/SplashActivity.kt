package com.app.rudo.view.splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.app.rudo.BuildConfig
import com.app.rudo.R
import com.app.rudo.utils.AppPrefrences
import com.app.rudo.view.MainActivity
import com.app.rudo.view.auth.LoginActivity
import kotlinx.android.synthetic.main.activity_splash.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein

class SplashActivity : AppCompatActivity(),KodeinAware {

    override val kodein by kodein()
    private var preferance : AppPrefrences? = null
    private var mDelayHandler: Handler? = null
    private val SPLASH_DELAY: Long = 3000 //3 seconds
    private val versionName = BuildConfig.VERSION_NAME
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        preferance = AppPrefrences(applicationContext)
        rippleBackground.startRippleAnimation()
        setUpRunnable()
    }
    fun setUpRunnable(){
        //Initialize the Handler
        mDelayHandler = Handler()
        //Navigate with delay
        mDelayHandler!!.postDelayed(mRunnable, SPLASH_DELAY)
    }

    override fun onDestroy() {
        super.onDestroy()
        rippleBackground.stopRippleAnimation()
    }

    internal val mRunnable: Runnable = Runnable {
        if (!isFinishing) {
            val accessToken = preferance?.getAccessToken()
            if(accessToken.isNullOrEmpty()) {
                Intent(applicationContext, LoginActivity::class.java).also {
                    it.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                    applicationContext.startActivity(it)
                    finish()
                }
            }else{
                Intent(applicationContext, MainActivity::class.java).also {
                    it.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                    applicationContext.startActivity(it)
                    finish()
                }
               /* Intent(applicationContext, HomeActivity::class.java).also {
                    it.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                    applicationContext.startActivity(it)
                    finish()
                }*/
            }
        }
    }
}