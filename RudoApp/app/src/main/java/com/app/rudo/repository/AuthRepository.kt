package com.app.rudo.repository

import com.app.rudo.BuildConfig
import com.app.rudo.contrants.Constants
import com.app.rudo.model.AuthResponse
import com.app.rudo.network.ApiRequest
import com.app.rudo.network.api.RudoApi

// Created by Satyabrata Bhuyan on 29-06-2020.
// Company  Yutu Electronics
// E_Mail   s.bhuyan0037@gmail.com

class AuthRepository(
    private val api: RudoApi
) : ApiRequest() {

    suspend fun userLogin(username: String, password: String):AuthResponse {
        return apiRequest { api.userLogin(
            BuildConfig.CLIENT_ID,
            BuildConfig.CLIENT_SECRET,
            "password",
            Constants.PREFIX_USERNAME+username,
            password,
            BuildConfig.REDIRECT_URI,
            BuildConfig.REDIRECT_OPENTT_URI+"oauth2/token")!! }
    }
    suspend fun userLoginWithOpenTT(username: String, password: String): AuthResponse {
        return apiRequest { api.userLoginWithOprnTTApi(
            BuildConfig.CLIENT_ID,
            BuildConfig.CLIENT_SECRET,
            "password",
            Constants.PREFIX_USERNAME+username,
            password,
            BuildConfig.REDIRECT_URI)!! }
    }

    suspend fun userSignup(
        name: String,
        password: String,
        date: Long
    ) : AuthResponse {
        return apiRequest{ api.userSignup(
            BuildConfig.CLIENT_ID,
            BuildConfig.CLIENT_SECRET,
            name,
            password,
            date,
            BuildConfig.REDIRECT_URI,
            BuildConfig.REDIRECT_OPENTT_URI+"v3/user/register"
        )!!}
    }
    suspend fun userSignupWithOpenTT(
        name: String,
        password: String,
        date: Long
    ) : AuthResponse {
        return apiRequest{ api.userSignupWithOpenTTApi(
            BuildConfig.CLIENT_ID,
            BuildConfig.CLIENT_SECRET,
            name,
            password,
            date,
            BuildConfig.REDIRECT_URI
        )!!}
    }

    suspend fun forgetPassword(username: String, password: String):AuthResponse {
        return apiRequest { api.forgotPassword(
            BuildConfig.CLIENT_ID,
            BuildConfig.CLIENT_SECRET,
            Constants.PREFIX_USERNAME+username,
            password,
            System.currentTimeMillis(),
            BuildConfig.REDIRECT_OPENTT_URI+"/v3/user/resetPassword")!! }
    }
    suspend fun forgetPasswordWithOpenTT(username: String, password: String): AuthResponse {
        return apiRequest {
            api.forgotPasswordWothOpenTTApi(
            BuildConfig.CLIENT_ID,
            BuildConfig.CLIENT_SECRET,
            Constants.PREFIX_USERNAME+username,
            password,
            System.currentTimeMillis())!! }
    }


    // suspend fun saveUser(user: User) = db.getUserDao().upsert(user)

    //fun getUser() = db.getUserDao().getuser()

}