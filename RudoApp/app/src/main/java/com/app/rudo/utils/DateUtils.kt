package com.app.rudo.utils

import android.R
import android.app.TimePickerDialog
import android.content.Context
import android.os.Build
import android.widget.TextView
import androidx.annotation.RequiresApi
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*
import kotlin.time.ExperimentalTime

/*
// Created by Satyabrata Bhuyan on 09-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

class DateUtils {
    @Suppress("DEPRECATION")
    companion object {
        @RequiresApi(Build.VERSION_CODES.O)
        fun getDateAndTime(dateTime: Long): String {
            try {
                val sp = SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS")
                val date = Date(dateTime)
                val t = sp.format(date)
                return t.toString()
            } catch (e: Exception) {

            }
            return ""
        }

        fun getCurrentDateTime(endTime: String): Long {
            val c = Calendar.getInstance()
            if ("endTime" == endTime) {
                c.add(Calendar.HOUR, 1)
                c.add(Calendar.MINUTE, 0)
                c.add(Calendar.SECOND, 0)
            }
            return c.time.time
        }

        fun getFormatedDate(c: Long): String {
            val date = Date(c)
            val sp = SimpleDateFormat("YYYY-MM-dd HH:MM:SS", Locale.getDefault())
            return sp.format(date).toString()
        }

        fun getFormatedDateAndTime(c: Long): String {
            // val date =Date(c)
            val pattern = "yyyy-MM-dd HH:mm"
            val simpleDateFormat = SimpleDateFormat(pattern)
            val dateNew = simpleDateFormat.format(Date(c))
            return dateNew.toString()
        }

        @ExperimentalTime
        fun getSeconds(timeLong: Long): String {
            val date = Date(timeLong)
            val sp = SimpleDateFormat("S", Locale.getDefault())
            return sp.format(date).toString() + "s"
        }

        fun getTimeForPasscodeFormart(): Long {
            var result = 0L
            try {
                val c = Calendar.getInstance()
                c.add(Calendar.HOUR, 0)
                return c.time.time
            } catch (e: Exception) {
                Timber.e("Error message", e.message)
            }
            return result
        }

        fun getTimePicker(textView: TextView, context: Context) {
            val cal = Calendar.getInstance()
            val timeSetListener = TimePickerDialog.OnTimeSetListener { timePicker, hour, minute ->
                cal.set(Calendar.HOUR, hour)
                cal.set(Calendar.MINUTE, minute)

                val temp = getFormatedDateAndTime(cal.time.time)
                textView.text = temp

            }
            TimePickerDialog(
                context,
                R.style.Theme_Holo_Light_Dialog,
                timeSetListener,
                cal.get(Calendar.HOUR),
                cal.get(Calendar.MINUTE),
                true
            ).show()
        }
    }
}