package com.app.rudo.view.settings.lock

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.app.rudo.R
import com.app.rudo.databinding.FragmentBasicBinding
import com.app.rudo.model.lockdetails.LockDetails
import com.app.rudo.view.settings.SettingViewModel
import com.app.rudo.view.settings.SettingViewmodelFactory
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

/*
// Created by Satyabrata Bhuyan on 25-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

class BasicOptionFragment:Fragment(), KodeinAware {
    override val kodein: Kodein by kodein()
    val factory: SettingViewmodelFactory by instance<SettingViewmodelFactory>()
    var viewModel: SettingViewModel? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentBasicBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_basic, container, false)
        viewModel = ViewModelProvider(this, factory).get(SettingViewModel::class.java)
        binding.basicvm = viewModel
        //viewModel?.registerListner(this)
        arguments?.let {
            val lockdetails = (LockDetails::class.java).cast(arguments?.getSerializable("lockdetails"))
            viewModel?.lockDetails = lockdetails
            binding.lockdetails = lockdetails
        }
        return binding.root
    }
}