package com.app.rudo.view.lockdetails.access

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.app.rudo.R
import com.app.rudo.databinding.FragmentGiveOntimeBinding
import com.app.rudo.utils.hide
import com.app.rudo.utils.hideKeyboard
import com.app.rudo.utils.show
import com.app.rudo.utils.snackbar
import kotlinx.android.synthetic.main.fragment_give_ontime.*
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

/*
// Created by Satyabrata Bhuyan on 06-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

class GiveOneTimeFragment(private var lockId: Int) : Fragment(), GiveAccessImpl, KodeinAware {
    override val kodein: Kodein by kodein()
    private val factory: GiveAccessViewModelFactory by instance<GiveAccessViewModelFactory>()
    private var viewModel: GiveAccessViewModel? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentGiveOntimeBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_give_ontime,
            container,
            false
        )
        viewModel = ViewModelProvider(this, factory).get(GiveAccessViewModel::class.java)
        viewModel?.registerListner(this)
        binding.viewmodel = viewModel
        viewModel?.lockId = lockId
        return binding.root
    }

    override fun onStarted() {
        context?.hideKeyboard(view?.rootView!!)
        progressBarGiveOnetime.show()
    }

    override fun onSuccess() {
        edtRecipients.setText("")
        edtEkeyName.setText("")
        progressBarGiveOnetime.hide()
        view?.rootView?.snackbar("Successfully share the eAccess")
    }

    override fun onFailure(message: String) {
        progressBarGiveOnetime.hide()
        view?.rootView?.snackbar(message)
    }
}